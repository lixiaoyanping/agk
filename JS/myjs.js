
// Tabs
function openLink(evt, linkName) {
  var i, x, tablinks;
  if (linkName == "English") {
	  x = document.getElementsByClassName("mycantonese");
	  for (i = 0; i < x.length; i++) {
		  x[i].style.display = "none";
	  }
      document.getElementById(linkName).style.display = "block";
  } 
  else {
	  x = document.getElementsByClassName("myenglish");
    for (i = 0; i < x.length; i++) {
		  x[i].style.display = "none";
	}
	  document.getElementById(linkName).style.display = "block";
  }
   document.getElementsByClassName("tablink")[0].click();
}


function loginredirect(){
  var user_name = document.getElementById("username_en").value;
  var result = document.getElementById("result").value;
  var password = document.getElementById("password_en").value;
  var remember_me = document.getElementById('remember_me_en').checked;
  var remember = readCookie('rememberme');
  if (remember) {
     window.location = "aspx/index_en.aspx";
  } else {
     if (user_name != '' && (user_name=='abc' || user_name=='test'|| user_name=='admin') 
		   && user_name != '' && (password == 'abc' || password == 'test' || password == 'admin')) {
            if (result == "Passed") {
                if (remember_me) {
                     var date = new Date();
                     date.setTime(date.getTime()+(60*1000)); //60 seconds
                     var expires = "; expires="+date.toGMTString();
                     document.cookie = "rememberme=true"+expires+"; path=/";
                }
                window.location = "aspx/index_en.aspx";

            } else
                alert("Please complete the verification");
        } else {
			    alert("Username or Password incorrect");
			
		}
    }
}

function loginredirect_cn(){
  var user_name_ca = document.getElementById("username_ca").value;
  var result = document.getElementById("result").value;
  var password_ca = document.getElementById("password_ca").value;
  var remember_me = document.getElementById('remember_me_ca').checked;
  var remember = readCookie('rememberme');
  if (remember) {
     window.location = "aspx/index.aspx";
  } else {
     if (user_name_ca != '' && (user_name_ca=='abc' || user_name_ca=='test'|| user_name_ca=='admin') 
		   && password_ca != '' && (password_ca == 'abc' || password_ca == 'test' || password_ca == 'admin')) {
		    //alert("Login Successful");	
            if (result == "Passed") {
				if (remember_me) {
                     var date = new Date();
                     date.setTime(date.getTime()+(60*1000));
                     var expires = "; expires="+date.toGMTString();
                     document.cookie = "rememberme=true"+expires+"; path=/";
                }
                window.location = "aspx/index.aspx";
            } else
                alert("Please complete the verification");
        } else {
			    alert("Username or Password incorrect");
			
		}
    }
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function callback() {
  var x = document.getElementById("result").value = "Passed";
}
