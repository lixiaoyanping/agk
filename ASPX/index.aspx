﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="ASPX_index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
	<style type="text/css">
		.auto-style1 {
			width: 138px;
			height: 129px;
		}
	</style>

	<link href="../CSS/style.css" rel="stylesheet" type="text/css" />
	<link href="../CSS/chosen.css" rel="stylesheet" />
</head>
<body bgcolor="#e3ffff">
    <form id="form1" runat="server">
		<div id="Parent">
	<div class="left">
		<em>ID: </em>
		<select data-placeholder="12345" class="chosen-select dropdown" tabindex="2">
		<option value=""></option>
		<option value="12345">12345</option>
		<option value="12346">12346</option>
		<option value="12345">12345</option>
		<option value="12345">12434</option>
		<option value="1456">14567</option>
		<option value="54321">54321</option>
		<option value="12341">12341</option>
		<option value="12343">12343</option>
		<option value="12337">12337</option>
		<option value="12434">12434</option>
		<option value="14567">14567</option>
		<option value="54321">54321</option>
	</select>
	</div>
    <div class="right">
    
    	<img id="logo" alt="" src="../Images/logoPGK.png" />

    </div>

        <div class="divtopleft">
        <ul class="ul">
			<li class="li"><a class="active" href="#home">首頁</a></li>
			<li class="li"><a href="#個人信息">個人信息</a></li>
			<li class="li"><a href="#我的團隊">我的團隊</a></li>
			<li class="li"><a href="#積分信息">積分信息</a></li>
            <li class="li"><a href="#信息中心">信息中心</a></li>
			<li class="li"><a href="#AGK交易市場">AGK交易市場</a></li>
        </ul>
        </div>
        <div class="divcentre">
        <ul class="ul">
            <li class="li2">ID 12345</li>
            <li class="li2"><a href="English">英文版</a></li>
			<li class="li2"><a href="Logout">退出</a></li>
		</ul>
        </div>
		<div class="divtopright">&nbsp;</div>
	<br /><br /><br /><br />
    <div class="divmain">
		<div class="divleft">
		
            <div id="main1">
			  <div class="divbox"><a href="box1" target="_self"><img class="boximage" src="../images/box1.png" alt="box1" onmouseover="this.src='../images/box1_olay.png'" onmouseout="this.src='../images/box1.png'"/></a></div>
			  <div class="divbox"><a href="box2" target="_self"><img class="boximage" src="../images/box2.png" alt="box2" onmouseover="this.src='../images/box2_olay.png'" onmouseout="this.src='../images/box2.png'"/></a></div>
			  <div class="divbox"><a href="box3" target="_self"><img class="boximage" src="../images/box3.png" alt="box3" onmouseover="this.src='../images/box3_olay.png'" onmouseout="this.src='../images/box3.png'"/></a></div>
			</div>

            <div id="main2">
				<div class="divbox"><a href="box4" target="_self"><img class="boximage" src="../images/box4.png" alt="box4" onmouseover="this.src='../images/box4_olay.png'" onmouseout="this.src='../images/box4.png'"/></a></div>
				<div class="divbox"><a href="box5" target="_self"><img class="boximage" src="../images/box5.png" alt="box5" onmouseover="this.src='../images/box5_olay.png'" onmouseout="this.src='../images/box5.png'"/></a></div>
				<div class="divbox"><img class="boximage" src="../images/box6.png" "/></div>
            </div>
		</div>
		<div class="divright">
			<div class="divmessage">
				<strong> <font size="5">我的信息 </strong>
				<hr />
				<div class="boxes">
                  <div class="row">
					<div class="column">AGK：</div>
					<div class="column">&nbsp</div>
					<div class="column">EP余額：</div>
					<div class="column">&nbsp</div>
				  </div>
				  <div class="row">
					<div class="column">購鑰匙幣：</div>
					<div class="column">&nbsp</div>
					<div class="column">注冊積分：</div>
					<div class="column">&nbsp</div>
				  </div>
				  <div class="row">
					<div class="column">玩家級別：</div>
					<div class="column">&nbsp</div>
					<div class="column">&nbsp</div>
					<div class="column">&nbsp</div>
				  </div>
				  <div class="row">
					<div class="column">開通時間：</div>
					<div class="column">&nbsp</div>
					<div class="column">上次登錄時間：</div>
					<div class="column">&nbsp</div>
				  </div>
                </div>
			</div>		
		</div>
	</div>
			</div>
    
    <div id="divcircle">
		<img src="../Images/round1.png" />
		<img src="../Images/arrow.png" />
		<img src="../Images/round2.png" />
		<img src="../Images/arrow.png" />
		<img src="../Images/round3.png" />
		<img src="../Images/arrow.png" />
		<img src="../Images/round4.png" />
		<img src="../Images/arrow.png" />
		<img src="../Images/round5.png" />
		<img src="../Images/arrow.png" />
		<img src="../Images/round6.png" />
    </div> 
	<br />	<br /><br />
	<div id="bottombar"><br /><font size="3">A Golden Key.INC 2014.保留所有權 <br />&nbsp </div> 
        

`	<script src="../JS/jquery.js"></script>
	<script src="../JS/prism.js"  charset="utf-8"></script>
	<script src="../JS/chosen.jquery.js"></script>
	<script src="../JS/init.js"  charset="utf-8"></script>
    </form>
</body>
</html>
