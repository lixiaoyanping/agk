﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="ASPX_index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
	<style type="text/css">
		.auto-style1 {
			width: 138px;
			height: 129px;
		}
	</style>

	<link href="../CSS/style.css" rel="stylesheet" type="text/css" />
	<link href="../CSS/chosen.css" rel="stylesheet" />
</head>
<body bgcolor="#e3ffff">
    <form id="form1" runat="server">
		<div id="Parent">
	<div class="left">
		<em>ID: </em>
		<select data-placeholder="12345" class="chosen-select dropdown" tabindex="2">
		<option value=""></option>
		<option value="12345">12345</option>
		<option value="12346">12346</option>
		<option value="12345">12345</option>
		<option value="12345">12434</option>
		<option value="1456">14567</option>
		<option value="54321">54321</option>
		<option value="12341">12341</option>
		<option value="12343">12343</option>
		<option value="12337">12337</option>
		<option value="12434">12434</option>
		<option value="14567">14567</option>
		<option value="54321">54321</option>
	</select>
	</div>
    <div class="right">
    
    	<img id="logo" alt="" src="../Images/logoPGK.png" />

    </div>

        <div class="divtopleft">
        <ul class="ul">
			<li class="li"><a class="active" href="#home">Home</a></li>
			<li class="li"><a href="#My Account">My Account</a></li>
			<li class="li"><a href="#My Group">My Group</a></li>
			<li class="li"><a href="#Game Score">Score Information</a></li>
            <li class="li"><a href="#Information Center">Information Center</a></li>
			<li class="li"><a href="#AGK Trading Market">AGK Market</a></li>
        </ul>
        </div>
        <div class="divcentre">
        <ul class="ul">
            <li class="li2">ID 12345</li>
            <li class="li2"><a href="Chinese">Chinese</a></li>
			<li class="li2"><a href="Logout">Logout</a></li>
		</ul>
        </div>
		<div class="divtopright">&nbsp;</div>
	<br /><br /><br /><br />
    <div class="divmain">
		<div class="divleft">
		
            <div id="main1">
			  <div class="divbox"><a href="box1" target="_self"><img class="boximage" src="../images/box_en1.png" alt="box1" onmouseover="this.src='../images/box_en1_olay.png'" onmouseout="this.src='../images/box_en1.png'"/></a></div>
			  <div class="divbox"><a href="box2" target="_self"><img class="boximage" src="../images/box_en2.png" alt="box2" onmouseover="this.src='../images/box_en2_olay.png'" onmouseout="this.src='../images/box_en2.png'"/></a></div>
			  <div class="divbox"><a href="box3" target="_self"><img class="boximage" src="../images/box_en3.png" alt="box3" onmouseover="this.src='../images/box_en3_olay.png'" onmouseout="this.src='../images/box_en3.png'"/></a></div>
			</div>

            <div id="main2">
				<div class="divbox"><a href="box4" target="_self"><img class="boximage" src="../images/box_en4.png" alt="box4" onmouseover="this.src='../images/box_en4_olay.png'" onmouseout="this.src='../images/box_en4.png'"/></a></div>
				<div class="divbox"><a href="box5" target="_self"><img class="boximage" src="../images/box_en5.png" alt="box5" onmouseover="this.src='../images/box_en5_olay.png'" onmouseout="this.src='../images/box_en5.png'"/></a></div>
				<div class="divbox"><img class="boximage" src="../images/box6.png" "/></div>
            </div>
		</div>
		<div class="divright">
			<div class="divmessage">
				<strong> <font size="5">Information </strong>
				<hr />
				<div class="boxes">
                  <div class="row">
					<div class="column">AGK：</div>
					<div class="column">&nbsp</div>
					<div class="column">EP Balance：</div>
					<div class="column">&nbsp</div>
				  </div>
				  <div class="row">
					<div class="column">Buy Key Money：</div>
					<div class="column">&nbsp</div>
					<div class="column">Point Registration：</div>
					<div class="column">&nbsp</div>
				  </div>
				  <div class="row">
					<div class="column">Player Level：</div>
					<div class="column">&nbsp</div>
					<div class="column">&nbsp</div>
					<div class="column">&nbsp</div>
				  </div>
				  <div class="row">
					<div class="column">Start Time：</div>
					<div class="column">&nbsp</div>
					<div class="column">Last Login Time：</div>
					<div class="column">&nbsp</div>
				  </div>
                </div>
			</div>		
		</div>
	</div>
			</div>
    
    <div id="divcircle">
		<img src="../Images/round1.png" />
		<img src="../Images/arrow.png" />
		<img src="../Images/round2.png" />
		<img src="../Images/arrow.png" />
		<img src="../Images/round3.png" />
		<img src="../Images/arrow.png" />
		<img src="../Images/round4.png" />
		<img src="../Images/arrow.png" />
		<img src="../Images/round5.png" />
		<img src="../Images/arrow.png" />
		<img src="../Images/round6.png" />
    </div> 
	<br />	<br /><br />
	<div id="bottombar"><br /><font size="3">A Golden Key.INC 2014.保留所有權 <br />&nbsp </div> 
        

`	<script src="../JS/jquery.js"></script>
	<script src="../JS/prism.js"  charset="utf-8"></script>
	<script src="../JS/chosen.jquery.js"></script>
	<script src="../JS/init.js"  charset="utf-8"></script>
    </form>
</body>
</html>
