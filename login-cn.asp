﻿<!DOCTYPE html>
<html>
<title>PGK</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://www.google.com/recaptcha/api.js"></script>
<script src="js/myjs.js"></script>  
<link rel="stylesheet" href="css/mystyle.css" type="text/css">

<style>
body,h1,h2,h3,h4,h5,h6 {font-family:"Raleway", Arial, Helvetica, sans-serif}
</style>

<body class="">
  <header class="" style="">
    <img class="w3-image" src="img/bg9-CN.png" alt="bg" style="">
    <div class="w3-display-right w3-padding w3-col l4 m8 logindivbg-en">
      <div>
        <h2><i class="fa fa-bed w3-margin-right"></i></h2>
      </div>
        <div class="w3-padding-16">
          <div class="w3-row-padding" style="margin:0 30px 25px 13px;">
            <div class="w3-margin-bottom">
              <hr>  
              <label class="logindiv-change"><i class=""></i> <a a href="login.asp">English</a>/繁体版
              </label>                   
              <input id="username_ca" class="w3-input w3-border w3-margin-1 w3-tiny m1 tuoyuan"  type="text;" placeholder="帳戶">
              <input id="password_ca" class="w3-input w3-border w3-margin-1 w3-tiny m1 tuoyuan" type="password" placeholder="密碼">
            </div> 
		    <div class="g-recaptcha w3-input  w3-border  w3-margin-1 w3-tiny  m1 tuoyuan"
			data-sitekey="6Ld-FCgUAAAAADjNywFKAantQkKH_MxR73Ta2qdk" data-callback="callback">
            </div>
              <input type="hidden" name="result" id="result" value="Failed">
              <input id="remember_me_ca" type="checkbox" value="Remember me" checked="checked" />記住我
              <button id="login_button" class="w3-button w3-orange tuoyuan keepright" onClick="loginredirect_cn()">登録
              </button>
          </div>
      </div>
    </div>
</header>
       
</body >
</html >
